#!/bin/bash
for test in `find tmp/userfiles -name "*.tst"`
do
	chipname=$(echo $test | cut -c '15-' | rev | cut -c '5-' | rev) #remove the first 14 characters and the last 4 to extract chip name
	printf "\t$chipname: " >> results.txt
	if [ ! -f tmp/userfiles/$chipname.hdl ]
	then
		echo "file not submitted" >> results.txt
	else
		sh nand2tetris_src/hardwareSimulator/HardwareSimulator.sh $test >> results.txt 2>> results.txt #both stdout and stderr go into results
	fi
done
