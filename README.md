# Automated Testing for CS140

This repository contains a simple automated testing system forthe *nand2tetris* projects of Clark University's CS140. Currently this testing system only supports the first three *nand2tetris* projects (01, 02, 03).

## Installation

Clone this repository to some folder on your computer.

## Usage

1. Download from Moodle the submissions as a zip file. Under the **view submissions** section of the assignment select from the grading action dropdown **download all submissions**. Place this zip file into the *submissions* folder of this repository. If such a folder does not exist please make one in the root directory of the repo (parallel to test_users.sh). Make sure that it is the only zip file in the submissions folder.
2. From the command line execute `sh test_users.sh project_number`, where *project_number* is the nand2tetris project number (01, 02, or 03).
3. Results will be generated into a textfile in this directory, `results.txt`.

## Expanding the system

Further projects can be supported but only if they involve comparison of user submitted *.hdl* files. Simply place the comparison files (*.cmp* and *.tst*) into another directory inside of `nand2tetris_src/projects` and run using this new directory name as the argument.
